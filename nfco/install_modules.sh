#!/bin/bash
for mod in netforce_*; do
    echo "installing $mod..."
    cd $mod
    ./setup.py develop
    cd ..
done
